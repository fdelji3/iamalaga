from django.db import models

# Create your models here.

class Post(models.Model):
    title=models.TextField(max_length=255, blank=True,null=True)
    content=models.TextField(blank=True,null=True)
    datetime_created=models.DateTimeField(auto_now_add=True, blank=True)
    image = models.FileField()
    
    class Meta:
        verbose_name="Post"
        verbose_name_plural="Posts"