

Vue.use(BootstrapVue);

var vm = new Vue({
    el: '#app',
    delimiters: ["[[","]]"],
    data() {
        return {
            posts:['primer post'],
            breadcrumbs: [{
                text: 'Home',
                href: '/'
              },
              {
                text: 'Manage',
                href: '#'
              },
              {
                text: 'Library',
                active: true
              }],
            show: true
        }
    },
    mounted: function () {
        
    },
    methods: {
        
    },

})