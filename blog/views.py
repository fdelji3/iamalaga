from django.shortcuts import render
from django.views import View
from blog.models import Post
# Create your views here.


class homeView(View):
    name = 'home'

    def get(self,request,*args,**kwargs):
        posts = Post.objects.all()
        return render(request,'home.html',{'posts':posts})